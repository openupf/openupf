# OpenUPF

兼容3GPP R16规范的开源UPF实现。

OpenUPF是用于第5代(5G)移动核心网络用户面功能的开源项目. 该项目的目标是实现3GPP Release 16(R16)及更高版本中定义的用户面功能网元(UPF).

该软件主要分为三个单元：LBU(负载平衡单元)，SMU(慢速匹配单元)和FPU(快速处理单元).

OpenUPF使用[DPDK](https://github.com/DPDK/dpdk.git) 技术实现高性能数据包转发.

## OpenUPF架构

![OpenUPF架构](/images/openupf_architecture.png)

## 目录结构

```
├── build  编译脚本
│   └── script
├── config  配置文件
│   ├── fpu  FPU配置文件目录
│   ├── smu  SMU配置文件目录
│   └── lbu  LBU配置文件目录
├── core  源代码目录
│   ├── include  头文件目录
│   ├── platform  平台相关源码
│   │   ├── buffer
│   │   ├── cli  交互式命令行
│   │   ├── debug  调试
│   │   ├── include
│   │   ├── log  日志
│   │   ├── ros
│   │   └── trans
│   ├── service
│   │   ├── common  公共源码
│   │   ├── fastpass  FPU模块源码
│   │   ├── loadbalancer  LBU模块源码
│   │   └── pfcp  SMU模块源码
│   └── util  基础库
├── doc  文档目录
├── images  图片目录
├── install  安装目录
└── libs  依赖库
```

## 特性列表

|**Supported**|**UP Function Features**                                                                              |
|:-------:|----------------------------------------------------------------------------------------------------------|
| **Y**   |(**BUCP**)Downlink Data Buffering in CP function is supported by the UP function.                         |
| **Y**   |(**DDND**)The buffering parameter 'Downlink Data Notification Delay' is supported by the UP function.     |
| **Y**   |(**DLBD**)The buffering parameter 'DL Buffering Duration' is supported by the UP function.                |
| **Y**   |(**TRST**)Traffic Steering is supported by the UP function.                                               |
| **Y**   |(**FTUP**)F-TEID allocation / release in the UP function is supported by the UP function.                 |
| **Y**   |(**PFDM**)The PFD Management procedure is supported by the UP function.                                   |
| **Y**   |(**HEEU**)Header Enrichment of Uplink traffic is supported by the UP function.                            |
| **Y**   |(**TREU**)Traffic Redirection Enforcement in the UP function is supported by the UP function.             |
| **Y**   |(**EMPU**)Sending of End Marker packets supported by the UP function.                                     |
| **Y**   |(**PDIU**)Support of PDI optimised signalling in UP function.                                             |
| **Y**   |(**UDBC**)Support of UL/DL Buffering Control.                                                             |
| **Y**   |(**QUOAC**)The UP function supports being provisioned with the Quota Action to apply when reaching quotas.|
|   N     |(**TRACE**)The UP function supports Trace.                                                                |
| **Y**   |(**FRRT**)The UP function supports Framed Routing.                                                        |
| **Y**   |(**PFDE**)The UP function supports a PFD Contents including a property with multiple values.              |
| **Y**   |(**EPFAR**)The UP function supports the Enhanced PFCP Association Release feature.                        |
| **Y**   |(**DPDRA**)The UP function supports Deferred PDR Activation or Deactivation.                              |
| **Y**   |(**ADPDP**)The UP function supports the Activation and Deactivation of Pre-defined PDRs.                  |
| **Y**   |(**UEIP**)The UPF supports allocating UE IP addresses or prefixes.                                        |
| **Y**   |(**SSET**)UPF support of PFCP sessions successively controlled by different SMFs of a same SMF Set.       |
|   N     |(**MNOP**)UPF supports measurement of number of packets which is instructed with the flag 'Measurement of Number of Packets' in a URR.|
|   N     |(**MTE**)UPF supports multiple instances of Traffic Endpoint IDs in a PDI.                                |
|   N     |(**BUNDL**)PFCP messages bunding is supported by the UP function.                                         |
|   N     |(**GCOM**)UPF support of 5G VN Group Communication.                                                       |
|   N     |(**MPAS**)UPF support for multiple PFCP associations to the SMFs in an SMF set.                           |
|   N     |(**RTTL**)The UP function supports redundant transmission at transport layer.                             |
|   N     |(**VTIME**)UPF support of quota validity time feature.                                                    |
|   N     |(**NORP**)UP function support of Number of Reports.                                                       |
|   N     |(**IPTV**)UPF support of IPTV service                                                                     |
|   N     |(**IP6PL**)UPF supports UE IPv6 address(es) allocation with IPv6 prefix length other than default /64 (including allocating /128 individual IPv6 addresses).|
|   N     |(**TSCU**)Time Sensitive Communication is supported by the UPF.                                           |
|   N     |(**MPTCP**)UPF support of MPTCP Proxy functionality.                                                      |
|   N     |(**ATSSS-LL**)UPF support of ATSSS-LLL steering functionality.                                            |
|   N     |(**QFQM**)UPF support of per QoS flow per UE QoS monitoring.                                              |
|   N     |(**GPQM**)UPF support of per GTP-U Path QoS monitoring.                                                   |
|   N     |(**MT-EDT**)SGW-U support of reporting the size of DL Data Packets.                                       |
|   N     |(**CIOT**)UPF support of CIoT feature, e.g. small data packet rate enforcement.                           |
|   N     |(**ETHAR**)UPF support of Ethernet PDU Session Anchor Relocation.                                         |
|   N     |(**DDDS**)UPF support of reporting the first buffered / first discarded downlink data after buffering / directly dropped downlink data for downlink data delivery status notification.|
|   N     |(**RDS**)UP function support of Reliable Data Service                                                     |
|   N     |(**RTTWP**)UPF support of RTT measurements towards the UE Without PMF.                                    |
|   N     |(**QUASF**)The UP function supports being provisioned in a URR with an Exempted Application ID for Quota Action or an Exempted SDF Filter for Quota Action which is to be used when the quota is exhausted.|
|   N     |(**NSPOC**)UP function supports notifying start of Pause of Charging via user plane.                      |

## Summary

  - [快速启动](#getting-started)
  - [版权](#license)
  - [联系我们](#contact-us)

## 快速启动

请参考[部署安装文档](http://www.openupf.net/openupf_dg.pdf)


## 版权

[Apache-2.0](LICENSE)


## 贡献

  - 面向公众开放提交更新和改进
  - 欢迎在issues中提出问题和建议
  - 欢迎广泛宣传
  - 欢迎任何建议

## 支持
   Copyright © 2021 深圳派纳斯科技有限公司
   
## 联系我们
   * 网址: [openupf.net](http://www.openupf.net)


